package ch.verttigo.sakura.loadbalancer;

import cloud.timo.TimoCloud.api.TimoCloudAPI;
import cloud.timo.TimoCloud.api.events.EventHandler;
import cloud.timo.TimoCloud.api.events.Listener;
import cloud.timo.TimoCloud.api.events.player.PlayerConnectEvent;
import cloud.timo.TimoCloud.api.events.player.PlayerDisconnectEvent;
import cloud.timo.TimoCloud.api.objects.ServerObject;
import cloud.timo.TimoCloud.api.plugins.TimoCloudPlugin;

import java.util.Timer;
import java.util.TimerTask;

public class LoadBalancer extends TimoCloudPlugin implements Listener {

    public static Timer shutDownTimerFallback = new Timer();
    public static Timer shutDownTimerLobby = new Timer();
    public static Boolean inCDFallback = false;
    public static Boolean inCDLobby = false;

    @Override
    public void onLoad() {
        System.out.println("Sakura Load Balancer has been enabled!");
        TimoCloudAPI.getEventAPI().registerListener(new LoadBalancer());
    }

    @Override
    public void onUnload() {
        System.out.println("Sakura Load Balancer has been disabled!");
    }

    @EventHandler
    public void onPlayerJoin(PlayerConnectEvent e) {
        managerServer(35, "Lobby");
        managerServer(150, "Fallback");
    }

    @EventHandler
    public void onPlayerDisconnect(PlayerDisconnectEvent e) {
        managerServer(35, "Lobby");
        managerServer(150, "Fallback");
    }


    public static int getOnlineAmountServer(String serverType) {
        return TimoCloudAPI.getUniversalAPI().getServerGroup(serverType).getOnlineAmount();
    }

    public static void setServerSize(int amount, String serverType) {
        TimoCloudAPI.getUniversalAPI().getServerGroup(serverType).setOnlineAmount(amount);
    }


    public static void shutdownTask(int ratio, String serverType) {
        if (serverType == "Lobby") {
            shutDownTimerLobby.schedule(new TimerTask() {
                @Override
                public void run() {
                    if (getOnlineAmountServer(serverType) > getRatio(ratio)) {
                        setServerSize(getRatio(ratio), serverType);
                    }
                    inCDLobby = false;
                }
            }, 300 * 1000);
        } else if (serverType == "Fallback") {
            shutDownTimerFallback.schedule(new TimerTask() {
                @Override
                public void run() {
                    if (getOnlineAmountServer(serverType) > getRatio(ratio)) {
                        setServerSize(getRatio(ratio), serverType);
                    }
                    inCDFallback = false;
                }
            }, 300 * 1000);
        }
    }


    public static int getOnlinePlayerCounts() {
        int count = 0;
        for (ServerObject server : TimoCloudAPI.getUniversalAPI().getServers()) {
            count += server.getOnlinePlayerCount();
        }
        return count;
    }

    public static int getRatio(int ratioNeeded) {
        int count = (getOnlinePlayerCounts() / ratioNeeded) + 1;
        if (count == 0) count = 1;
        return count;
    }

    public static void managerServer(int ratioNeeded, String serverType) {
        boolean cd = false;

        if (inCDLobby || inCDFallback) {
            cd = true;
        }
        if (getRatio(ratioNeeded) < getOnlineAmountServer(serverType)) {
            if (!cd) {
                shutdownTask(ratioNeeded, serverType);
            }
            return;
        }

        if (getRatio(ratioNeeded) > getOnlineAmountServer(serverType)) {
            if (!cd) {
                setServerSize(getRatio(ratioNeeded), serverType);
            } else {
                if (serverType == "Lobby" && inCDLobby) {
                    inCDLobby = false;
                    shutDownTimerLobby.cancel();
                } else if (serverType == "Fallback" && inCDFallback) {
                    inCDFallback = false;
                    shutDownTimerFallback.cancel();
                }
            }
            return;
        }
    }
}